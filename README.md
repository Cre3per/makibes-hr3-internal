# Makibes HR3 disassembly

## Hardware

The case is glued all around. The black cover can be pried out or lifted out
if the glue has been softened. The plastic is soft, it's easy to penetrate it
with a screwdriver and damage the electronics.

![case](./res/case1.jpg)
![case](./res/case2.jpg)
![case](./res/case4.jpg)
![case](./res/cover.jpg)


The board is held in place by one clip at the top and another one at the bottom

![case](./res/case3.jpg)

The board is glued to the battery but comes right off.

![board_glue](./res/board1.jpg)

The battery is glued to the bottom of the case.
The heart rate sensor connects to the board using a flat flex. The connector is
under the display.

![battery_glue](./res/battery1.jpg)

The display connects to the board with a flat flex cable. The USB connector
connects the the board though pins. No desoldering needed.

![display_flatflex](./res/display_flatflex.jpg)


Battery: 3.7V 90mAh.

![battery](./res/battery.jpg)


Chip: HS6620D by Huntersun

![chip](./res/chip.jpg)

## Software

Write to service `6e400001-b5a3-f393-e0a9-e50e24dcca9e` (Nordic UART), characteristic
`6e400002-b5a3-f393-e0a9-e50e24dcca9e` (RX).

Each write is structured as follows

```
ab 00 [argument_count] ff [command] 80 [arguments]
```
where `[argument_count]` is `[arguments].length + 3`

See [Gadgetbridge](https://github.com/Cre3per/Gadgetbridge/blob/master/app/src/main/java/nodomain/freeyourgadget/gadgetbridge/devices/makibeshr3/MakibesHR3Constants.java#L79) for commands and arguments.